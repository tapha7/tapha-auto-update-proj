<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ip extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ip';
    protected $fillable = ['ip_start','ip_end', 'continent', 'country', 'stateprov', 'district', 'city', 'zipcode', 'latitude', 'longitude', 'geoname_id', 'timezone_offset', 'timezone_name', 'weather_code', 'isp', 'asNumber', 'usageType', 'asName']; 
}
