<?php

namespace App\Imports;

use App\Ip;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithProgressBar;

class IpImport implements ToModel, WithProgressBar
{
    use Importable;
    
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Ip([
                'ip_start' => $row[0],
                'ip_end' => $row[1], 
                'continent' => $row[2],
                'country' => $row[3],
                'stateprov' => $row[4], 
                'district' => $row[5],
                'city' => $row[6],
                'zipcode' => $row[7], 
                'latitude' => $row[8],
                'longitude' => $row[9],
                'geoname_id' => $row[10], 
                'timezone_offset' => $row[11],
                'timezone_name' => $row[12],
                'weather_code' => $row[13], 
                'isp' => $row[14],
                'asNumber' => $row[15],
                'usageType' => $row[16], 
                'asName' => $row[17],
        ]);
    }
}
