<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DbipFetch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dbip:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch (download) dbip ip to location CSV';

    /**
     * Set private progress bar.
     *
     * @var string
     */
    private $bar;

    /**
     * Set private iterator.
     *
     * @var string
     */
    private $i;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //Instantiate Guzzle client
        $client = new \GuzzleHttp\Client();

        //Initiate request to the DB-IP API and retrieve the latest download URL 
        $request = $client->get('https://db-ip.com/account/adcda1fff413ac2395a751f7cb7fdd28706cc197/db/ip-to-location-isp/');
        $response = json_decode($request->getBody());
        $latest_url = $response->csv->url;

        //Download the latest CSV data from the URL
        $filename = 'latest-download.csv.gz';

        //Start the progress bar
        $this->bar = $this->output->createProgressBar();
        $this->info($filename.' is downloading. Please wait.');
        $this->bar->start();

        //Start the download
        $this->i = 0;
        $result = $client->request(
            'GET',
            $latest_url,
            [
                'progress' => function(
                    $downloadTotal,
                    $downloadedBytes,
                    $uploadTotal,
                    $uploadedBytes
                ) {
                    //Implement iterator so setMaxSteps is only run once
                    if ($this->i == 0)
                    {
                        $this->bar->setMaxSteps($downloadTotal);
                    }
                    $this->bar->advance();
                    $this->i + 1;
                },
                'save_to' => resource_path() . '/download/latest-download.csv.gz'
            ]
        );

        //End the progress bar
        $this->bar->finish();
        $this->info('Download Successfully Completed!');
    }
}
