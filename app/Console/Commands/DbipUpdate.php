<?php

namespace App\Console\Commands;

use App\Ip;
use App\Imports\IpImport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class DbipUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dbip:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update dbip ip to location CSV data in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (file_exists(resource_path() . '/unzipped'.'/latest-download.csv'))
        {
            //Import to database from CSV
            $this->output->title('Starting import');
            (new IpImport)->withOutput($this->output)->import(resource_path() . '/unzipped'.'/latest-download.csv');
            //Excel::import(new IpImport, resource_path() . '/unzipped'.'/latest-download.csv');
            $this->output->success('Import successful');
            unlink(resource_path() . '/unzipped'.'/latest-download.csv');
            //Remove current gzip file
            if (file_exists(resource_path() . '/download'.'/latest-download.csv.gz'))
            {
                unlink(resource_path() . '/download'.'/latest-download.csv.gz');
            }
            $this->info('Successfully updated the database!');
        }
        else {
            $this->info("No files to parse.");
            $this->info("Please run 'php artisan dbip:fetch' and then 'php artisan dbip:unzip' to get latest download and then unzip it.");
        }
    }
}
