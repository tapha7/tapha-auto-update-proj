<?php

namespace App\Console\Commands;

use VIPSoft\Unzip\Unzip;
use Illuminate\Console\Command;

class DbipUnzip extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dbip:unzip';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Unzip dbip ip to location CSV';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //Read zipped file from /download resource path and set unzipped path
        $file_location = resource_path() . '/download/latest-download.csv.gz';
        $unzipped_path = resource_path() . '/unzipped';
        $file_type = substr($file_location, strrpos($file_location, '.') + 1);
        $file_name = substr($file_location, strrpos($file_location, '/') + 1);

        //Check if file is set and then extract into unzipped path if so.
        if(file_exists($file_location)){
            //If it is a ZIP file run below code
            if ($file_type == 'zip'){
                $unzipper  = new Unzip();
                $filenames = $unzipper->extract($file_location, $unzipped_path);
                //Extract the files into download/unzipped 
                if(is_array($filenames)) //Show file names
                {
                    $this->info('File Successfully Unzipped!');
                } 
            }
            elseif ($file_type == 'gz') { //If it is a gZIP file run below code
                //Extract gZip file
                // Raising this value may increase performance
                $buffer_size = 4096; // read 4kb at a time
                $out_file_name = str_replace('.gz', '', $file_name);
                $out_file_name = $unzipped_path.'/'.$out_file_name;

                // Open our files (in binary mode)
                $file = gzopen($file_location, 'rb');
                $out_file = fopen($out_file_name, 'wb'); 

                // Keep repeating until the end of the input file
                while (!gzeof($file)) {
                    // Read buffer-size bytes
                    // Both fwrite and gzread and binary-safe
                    fwrite($out_file, gzread($file, $buffer_size));
                }

                // Files are done, close files
                fclose($out_file);
                gzclose($file);

                $this->info('File Successfully Unzipped!');
            }
         }
         else {
            $this->info('No file is set.');
            $this->info("Please run then 'php artisan dbip:fetch' command, to download the latest csv zip file.");
         }
    }
}
