<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DbipFetchTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testDbipFetchTest()
    {
        $this->artisan('dbip:fetch')
         ->expectsOutput('Download Successfully Completed!')
         ->assertExitCode(0);
    }
}
