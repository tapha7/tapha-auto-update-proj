# Egoditor Project (Backend) - Tapha

To install, simply clone this repository: `git clone https://tapha7@bitbucket.org/tapha7/tapha-auto-update-proj.git` and then `cd` in to the project directory and run `composer install`.

## Available Commands

### dbip:fetch

`php artisan dbip:fetch`

This command fetch's (download) the dbip ip to location CSV.

[Watch it in action](https://www.loom.com/share/206086483ffc46c58b6f6a9c1a8be7a9)

### dbip:unzip

`php artisan dbip:unzip`

This command unzips dbip ip to location CSV.

[Watch it in action](https://www.loom.com/share/f0f8eae634bc490594afb34c5f0fd361)

### dbip:update

`php artisan dbip:update`

This command updates the dbip ip to location CSV data in database.

[Watch it in action](https://www.loom.com/share/26d5fc670e394f25953b6a49f4f45ce3)
