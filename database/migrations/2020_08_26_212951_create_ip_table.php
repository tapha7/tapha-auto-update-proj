<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ip', function (Blueprint $table) {
            $table->id();
            $table->string('ip_start')->nullable();;
            $table->string('ip_end')->nullable();;
            $table->string('continent')->nullable();;
            $table->string('country')->nullable();;
            $table->string('stateprov')->nullable();;
            $table->string('district')->nullable();;
            $table->string('city')->nullable();;
            $table->string('zipcode')->nullable();;
            $table->string('latitude')->nullable();;
            $table->string('longitude')->nullable();;
            $table->string('geoname_id')->nullable();;
            $table->string('timezone_offset')->nullable();;
            $table->string('timezone_name')->nullable();;
            $table->string('weather_code')->nullable();;
            $table->string('isp')->nullable();;
            $table->string('asNumber')->nullable();;
            $table->string('usageType')->nullable();;
            $table->string('asName')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ip');
    }
}